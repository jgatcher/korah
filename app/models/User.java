package models;

import java.util.*;
import play.data.validation.Constraints.*;
import com.google.code.morphia.annotations.*;
import leodagdag.play2morphia.Model;
import org.bson.types.ObjectId;

import com.google.code.morphia.mapping.Mapper;
import com.google.code.morphia.query.Query;
import com.google.code.morphia.query.QueryResults;
import com.google.code.morphia.query.UpdateOperations;
import com.google.code.morphia.query.UpdateResults;
import leodagdag.play2morphia.MorphiaPlugin;
//Ok design choice
//setup validation in here or validate in the controller eg add a tranisent confim password filed here so 
//in conroller i just call it instead of writing more logic
@Entity
public class User extends Model{
	@Id
	public ObjectId id;
     
     //@Required
    // @Email
	public String email;
    public String userType;

	 //@Required
	// @Min(6)
	public String password;
    public String firstName;
    public String otherName;
    public String lastName;
    public String address;
    public String homeNo;
    public String gender;
    public String mobileNo;
    public String maritalStatus;
    public String educationalLevel;
    public Date birthDate;

	 public static Model.Finder<ObjectId, User> find(){
    	return new Model.Finder<ObjectId, User>(ObjectId.class, User.class);

    }
    public static void create(User user){
    	user.insert();
    }//TODO move the low level stuff into an update method in morphia plugin
    public static void modify(User user,String id){
        ObjectId oid=ObjectId.massageToObjectId(id);
         System.out.println("Object id of the user to be updated "+oid);
       //  user.update(oid);
         Query<User> updateQuery = MorphiaPlugin.ds().createQuery(User.class).field(Mapper.ID_KEY).equal(oid);
       //  System.out.println(updateQuery);
        //UpdateOperations<User> ops=MorphiaPlugin.ds().createUpdateOperations(User.class).set("firstName",user.firstName).set("otherName",user.otherName).set("lastName",user.lastName).set("");
        UpdateOperations<User> ops=MorphiaPlugin.ds().createUpdateOperations(User.class).set("firstName",user.firstName).set("otherName",user.otherName).set("lastName",user.lastName).set("address",user.address).set("homeNo",user.homeNo).set("mobileNo",user.mobileNo).set("maritalStatus",user.maritalStatus).set("educationalLevel",user.educationalLevel).set("birthDate",user.birthDate).set("gender",user.gender);
         MorphiaPlugin.ds().update(updateQuery,ops);
        
        

    } 
    //Concatenates first and LastNames and returns a single String
    //builds the fullname step by step so resulting 
    //"fullname" could actually be one of the names only
    public String fullName(){
        StringBuilder fullName=new StringBuilder();
        if(firstName!=null && firstName.length()>0)
           fullName.append(fullName);
        if(lastName!=null && lastName.length()>0)
            fullName.append(" ");
            fullName.append(lastName);
        return fullName.toString();
    }

    public static List<User> all(){
    	return find().all();
    }
    
    public String userIdentifier(){
        if(fullName().isEmpty())
            return email;
        else
            return fullName();
    }

    public static User getUserById(ObjectId id){
    	return find().byId(id);
   	}

    public static String authenticate(String email,String password){
    	System.out.println(email+" "+password);
    	List<User> authenticated=find().filter("email",email).filter("password",password).asList();

    	if(authenticated.size()==1){
    		System.out.println(authenticated.get(0));
//    		authUser=authenticated.get(0);
    		return authenticated.get(0).id.toString();
    	}else return null;


    }
}