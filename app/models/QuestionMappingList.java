package models;

import java.util.*;//access to List 
import com.google.code.morphia.annotations.*;//@Annotations
import leodagdag.play2morphia.Model;
import org.bson.types.ObjectId;
import play.data.validation.Constraints.*;


public class QuestionMappingList extends Model{
	
	public ObjectId id;
    public String addMultipleQuestions;
	
	List<QuestionMapping> list;
	public String toString(){
		return "QuestionMappingList:\t"+list;
	}
}