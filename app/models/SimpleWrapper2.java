  package models;

import java.util.*;//access to List 
import com.google.code.morphia.annotations.*;//@Annotations
//import leodagdag.play2morphia.Blob;
import leodagdag.play2morphia.Model;
import org.bson.types.ObjectId;
import play.data.validation.Constraints.*;


@Entity
    public  class SimpleWrapper2 extends Model{
      @Embedded
      Simple simple;
      @Id 
      ObjectId id;
      String singleString;
      public List<String> multipleStrings=new ArrayList<String>();
      
      public void setSingleString(String singleString){
        this.singleString=singleString;
      }
      public String getSingleString(){
        return singleString;
      }
      public SimpleWrapper2(){}
      // public SimpleWrapper2(String singleString){
      //   this.singleString=singleString;
      // }
      @Override
      public String toString(){
        return "SimpleWrapper2[ SingleString:"+singleString+"\n MultipleStrings:"+multipleStrings+"]";
      }
    }