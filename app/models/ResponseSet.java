package models;

import java.util.*;//access to List 
import com.google.code.morphia.annotations.*;//@Annotations
//import leodagdag.play2morphia.Blob;
import leodagdag.play2morphia.Model;
import org.bson.types.ObjectId;
import play.data.validation.Constraints.*;

//collections15 contains generic versions
import org.apache.commons.collections15.Factory;
import org.apache.commons.collections15.map.LazySortedMap;



@Entity
public class ResponseSet extends Model{
	@Id
	public ObjectId id;
	// @Required
    public ObjectId userId;
    public ObjectId questionSetId;
    public boolean submitted;
	
    //Factory object used by lazymap to contruct default objects
    @Transient
    Factory<QResponse> factory = new Factory() {
        public QResponse create() {
         return new QResponse();
        }
      };
    @Transient
    TreeMap toBeDecorated =new TreeMap<Integer,QResponse>();
    
//    @Property
    @Embedded(concreteClass = java.util.TreeMap.class)
    public SortedMap<Integer,QResponse> responses=LazySortedMap.decorate(toBeDecorated,factory);
    //public TreeMap<String,String> responses=new TreeMap<String,String>();
   
    
    //Finder object that is used to retrieve posts each model class requres this
    public static Model.Finder<ObjectId,ResponseSet> find(){
    	return new Model.Finder<ObjectId,ResponseSet>(ObjectId.class,ResponseSet.class);
    }

    public static List<ResponseSet> all(){
    	
        return find().all();

    }

   
    public static ResponseSet getResponseSet(ObjectId id){
    	return find().byId(id);
    }
    //  public static String getQuestionNo(ObjectId questionSetId,ObjectId questionId){
    //     QuestionSet questionSet=QuestionSet.getQuestionSet(questionSetId);
        
        
    //     for(QuestionMapping qm:questionSet.attachedQuestions){
    //         if(qm.questionId.equals(questionId))
    //             return qm.questionNo;
    //     }return "QNo:";
    // }
    public static void create(ResponseSet responseSet){
    	responseSet.insert();

    }
      public static ResponseSet modify(ResponseSet responseSet,ObjectId id){
        //ObjectId oid=ObjectId.massageToObjectId(id);
         System.out.println("Object id of the ResponseSet to be updated "+id);
         return responseSet.update(id);
    }
    //refactor to single method..didnt feel like changing all the old code
     public static ObjectId createAndReturnId(ResponseSet responseSet){
        System.out.print(responseSet);
        ResponseSet returned=responseSet.insert();
        return returned.id;
    }
    // public void addResponse(QResponse response){
    // 	if(responses==null){
    //         responses=new ArrayList<QResponse>();
    //     }
    //     responses.add(response);
    // 	update();
    // }

    @Override
    public String toString(){
        return "ResponseSet{userId:"+userId+"Responses:"+responses+"}";
    }
}