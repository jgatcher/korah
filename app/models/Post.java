package models;

import java.util.*;//access to List 
import com.google.code.morphia.annotations.*;//@Annotations
//import leodagdag.play2morphia.Blob;
import leodagdag.play2morphia.Model;
import org.bson.types.ObjectId;
import play.data.validation.Constraints.*;


@Entity
public class Post extends Model{
	@Id
	public ObjectId id;
    public ObjectId authorId;
	
	public String title="Unittled";
	@Required
	public String content;
	
	// A post could be created but not available to others
	public boolean published;
	
	//not sure if auto timestamp is available as yet so 
	// will try to keep this updated manually with lifecycle methods
	public Date createdOn;
	public Date modifiedOn;
    public Date publishedOn;

    //Represent Entity relationship with annotation
    @Embedded
    public List<Comment> comments;
    
    //Finder object that is used to retrieve posts each model class requres this
    public static Model.Finder<ObjectId,Post> find(){
    	return new Model.Finder<ObjectId,Post>(ObjectId.class,Post.class);
    }

    public static List<Post> all(){
    	return find().all();
    }
    public static Post getPost(ObjectId id){
    	return find().byId(id);
    }
    public static void create(Post post){
    	
        post.insert();

    }

     @PrePersist void fillInTimestamp() {
      if (createdOn==null)  
          createdOn = new Date();
      else 
         modifiedOn = new Date(System.currentTimeMillis());
      if(authorId==null)
          authorId=controllers.Application.getAuthUser().id;
    }
    //return the name of the author if available otherwise email
    //if author is current user return me or maybe you?
    //pull this out into controller and make it availbe for all objects with authorlike props
    public String getPostAuthor(){
        User author=User.getUserById(authorId);
        if(authorId.equals(controllers.Application.getAuthUser().id)){
            return "me";
        }else {
            //the null check maybe unnecessary sine fullname always returns a string
            if(author.fullName()!=null && author.fullName().length()>0){
                return author.fullName();
            }
        }
        return author.email;
    }
    public static void modify(Post post,ObjectId id){
        //ObjectId oid=ObjectId.massageToObjectId(id);
         post.update(id);
    }
    public void insertComment(Comment comment){
    	if(comments==null){
            comments=new ArrayList<Comment>();
        }
        comments.add(comment);
    	update();
    }

}