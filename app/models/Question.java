package models;

import java.util.*;//access to List 
import com.google.code.morphia.annotations.*;//@Annotations
//import leodagdag.play2morphia.Blob;
import leodagdag.play2morphia.Model;
import org.bson.types.ObjectId;
import play.data.validation.Constraints.*;


@Entity
public class Question extends Model{
	@Id
	public ObjectId id;
    //public ObjectId questionSetId;
	@Required
    public String text;
    @Required
    public String qtype;
	public List<String> options=new ArrayList<String>();
    
    //Finder object that is used to retrieve posts each model class requres this
    public static Model.Finder<ObjectId,Question> find(){
    	return new Model.Finder<ObjectId,Question>(ObjectId.class,Question.class);
    }

    public static List<Question> all(){
    	return find().all();
    }
    public static Question getQuestion(ObjectId id){
    	System.out.print(find().byId(id));
        return find().byId(id);
    }

    public static void create(Question question){
    	question.insert();

    }
     public static ObjectId createAndReturnId(Question question){
        Question returned=question.insert();
        return returned.id;
    }
    public static void modify(Question question,ObjectId id){
        //ObjectId oid=ObjectId.massageToObjectId(id);
         System.out.println("Object id of the QuestionSet to be updated "+id);
         question.update(id);
    }
    public void addOption(String option){
    	if(options==null){
            options=new ArrayList<String>();
        }
        options.add(option);
    	update();
    }

}