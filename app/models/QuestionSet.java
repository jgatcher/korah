package models;

import java.util.*;//access to List 
import com.google.code.morphia.annotations.*;//@Annotations
//import leodagdag.play2morphia.Blob;
import leodagdag.play2morphia.Model;
import org.bson.types.ObjectId;
import play.data.validation.Constraints.*;

import com.google.code.morphia.mapping.Mapper;
import com.google.code.morphia.query.Query;
import com.google.code.morphia.query.QueryResults;
import com.google.code.morphia.query.UpdateOperations;
import com.google.code.morphia.query.UpdateResults;
import leodagdag.play2morphia.MorphiaPlugin;

@Entity
public class QuestionSet extends Model{
	@Id
	public ObjectId id;
    
	// @Required
    public String title;
    public String description;
    public ObjectId authorId;
    //NTS: Maybe this should be intialized ..it takes away the need to check for nulls
    public TreeMap<Integer,ObjectId> attachedQuestions=new TreeMap<Integer,ObjectId>();
    
    //Finder object that is used to retrieve posts each model class requres this
    public static Model.Finder<ObjectId,QuestionSet> find(){
    	return new Model.Finder<ObjectId,QuestionSet>(ObjectId.class,QuestionSet.class);
    }

    public static List<QuestionSet> all(){
    	return find().all();
    }
    public static QuestionSet getQuestionSet(ObjectId id){
    	return find().byId(id);
    }
    public static QuestionSet getQuestionSetByTitle(String title){
        List<QuestionSet>qs=find().filter("title",title).asList();
        if(qs.size()<1){
            return new QuestionSet();
        }
        return qs.get(0);
    }

    public static void create(QuestionSet questionSet){
    	questionSet.authorId=controllers.Application.getAuthUser().id;
        questionSet.insert();
    }
     /***
      Modifies only the desciptive info of a question Set
     */
     public static void modify(QuestionSet questionSet,ObjectId id){
        //ObjectId oid=ObjectId.massageToObjectId(id);
         System.out.println("Object id of the QuestionSet to be updated "+id);
         // questionSet.update(id);
         Query<QuestionSet> updateQuery = MorphiaPlugin.ds().createQuery(QuestionSet.class).field(Mapper.ID_KEY).equal(id);
       //  System.out.println(updateQuery);
        //UpdateOperations<User> ops=MorphiaPlugin.ds().createUpdateOperations(User.class).set("firstName",user.firstName).set("otherName",user.otherName).set("lastName",user.lastName).set("");
        UpdateOperations<QuestionSet> ops=MorphiaPlugin.ds().createUpdateOperations(QuestionSet.class).set("title",questionSet.title).set("description",questionSet.description);
         MorphiaPlugin.ds().update(updateQuery,ops);
    }
    /***
        Modifies the entire QuestionSet by baically replacing the current instance with the ont that is passed
    */
    // public void (){
    //   this
    // }
   
    public  String noOfAttachedQuestions(){
        
            return String.valueOf(attachedQuestions.size());
               
    }
    // public String nextQuestionNo(){

    //     return String.valueOf((attachedQuestions.size())+1);
    // }
       public Integer nextQuestionNo(){

        return attachedQuestions.size()+1;
    }
    /***
        Add a number of questions in a single operation

        it accepts a map as its param,so you need to create not just a map entry but a Map
        to insert of course a map with only one entry should work fine
    */
    public void addQuestions(SortedMap<Integer,ObjectId> questionMaps){//though of naeing it addQuestion but it may be confusing
        attachedQuestions.putAll(questionMaps);
        update();
    }
    /***
        Suppply question No and questionId to attach a questionMap
    */
    public void addQuestion(Integer questionNo,ObjectId questionId){
        attachedQuestions.put(questionNo,questionId);
        update();
    }

    @Override
    public String toString(){
        return "QuestionSet:{\n"+title+"\nattachedQuestions:"+attachedQuestions+"}";
    }

    /***I'm getting rid of the question Mapping class by utilizing inbuilt Map,should comein handy esp with the sorting issues 
    and also binding though that has been fixed ;-) i think
    */
    
    // public void addQuestion(QuestionMapping questionMap){
    // 	// if(attachedQuestions==null){
    //  //        attachedQuestions=new ArrayList<QuestionMapping>();
    //  //    }
    //     //TODO check for duplicates

    //     attachedQuestions.add(questionMap);
    // 	update();
    // }

}