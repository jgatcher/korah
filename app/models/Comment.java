package models;

import java.util.*;//access to List
import com.google.code.morphia.annotations.* ;
//import leodagdag.play2morphia.Blob;
import leodagdag.play2morphia.Model;
import org.bson.types.ObjectId;
import play.data.validation.Constraints.*;


@Entity
public class Comment extends Model{
	@Id
	public ObjectId id;
	@Required
	public String content;
    public ObjectId authorId;
	//Represent Entity relationship with annotation
    @Embedded
    public Post post;
    
	public Date createdOn;

	
    
    //Finder object that is used to retrieve posts each model class requres this
    public static Model.Finder<ObjectId,Comment> find(){
    	return new Model.Finder<ObjectId,Comment>(ObjectId.class,Comment.class);
    }
    public static List<Comment> all(){
    	return find().all();
    }
    
    @PrePersist void fillInTimestamp() {
      if (createdOn==null)  
          createdOn = new Date();
      if(authorId==null)
          authorId=controllers.Application.getAuthUser().id;
    }

}