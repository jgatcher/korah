package models;

import java.util.*;//access to List 
import com.google.code.morphia.annotations.*;//@Annotations
//import leodagdag.play2morphia.Blob;
import leodagdag.play2morphia.Model;
import org.bson.types.ObjectId;
import play.data.validation.Constraints.*;


 public class QuestionMapping extends Model{
   
       public ObjectId id;
        public String questionNo;
        public ObjectId questionId;
   
    public QuestionMapping(){}
    public QuestionMapping(String questionNo,ObjectId questionId){
    	this.questionNo=questionNo;
    	this.questionId=ObjectId.massageToObjectId(questionId);
    }
    public QuestionMapping(ObjectId questionId){
    	this.questionId=ObjectId.massageToObjectId(questionId);
    }
    @Override
    public String toString(){
    	return "QuestionMapping{questionNo:"+questionNo+"\nquestionId:"+questionId+"}";
    }
 }
    
    