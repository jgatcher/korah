  package models;

import java.util.*;//access to List 
import com.google.code.morphia.annotations.*;//@Annotations
//import leodagdag.play2morphia.Blob;
import leodagdag.play2morphia.Model;
import org.bson.types.ObjectId;
import play.data.validation.Constraints.*;
import org.apache.commons.collections15.Factory;
import org.apache.commons.collections15.functors.MapTransformer;      
import org.apache.commons.collections15.map.LazySortedMap;

  
  public class Simple {
   
    public String text;
    public List<String> stringList;
    Factory<SimpleWrapper2> factory = new Factory() {
        public SimpleWrapper2 create() {
         return new SimpleWrapper2();
        }
      };

    public Map<String,SimpleWrapper2> wrappedText=LazySortedMap.decorate(new TreeMap<String,SimpleWrapper2>(),factory);
    // public Simple(){
    //    wrappedText=
    // }
    
          
    //public TreeMap<String,SimpleWrapper> wrappedText=new LazySortedMap<String,SimpleWrapper>.de();

    
    @Override
    public String toString(){
      return text +"-"+wrappedText+"-"+stringList;
    }
   

    
  }