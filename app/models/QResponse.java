package models;

import java.util.*;//access to List 
import com.google.code.morphia.annotations.*;//@Annotations
//import leodagdag.play2morphia.Blob;
import leodagdag.play2morphia.Model;
import org.bson.types.ObjectId;
import play.data.validation.Constraints.*;


@Entity
public class QResponse extends Model{
	@Id
	public ObjectId id;
	// @Required
    public ObjectId questionId;
    @Embedded
    public ResponseSet responseSet;
	
    public String singleActualResponse;
    public List<String> actualResponses;
    
    public void setSingleActualResponse(){
        this.singleActualResponse=singleActualResponse;
    }
 

    public void setQuestionId(ObjectId questionId){
        this.questionId=questionId;
    }
    public void setActualResponse(String ... actualResponses){
        for(String s:actualResponses){
            this.actualResponses.add(s);
        }
    }

    
    //Finder object that is used to retrieve posts each model class requres this
    public static Model.Finder<ObjectId,QResponse> find(){
    	return new Model.Finder<ObjectId,QResponse>(ObjectId.class,QResponse.class);
    }

    public static List<QResponse> all(){
    	return find().all();
    }
    public static QResponse getResponse(ObjectId id){
    	return find().byId(id);
    }
    public static void create(QResponse response){
    	response.insert();

    }
    public void addActualResponse(String actualResponse){
    	if(actualResponses==null){
            actualResponses=new ArrayList<String>();
        }
        actualResponses.add(actualResponse);
    	update();
    }
    // @Override
    // public String toString(){
    //  return "Qresponse {questionId:"+questionId+" actualResponses"+actualResponses+"}";}

}