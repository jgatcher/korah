package controllers;

import play.*;
import play.mvc.*;
import models.*;
import views.html.*;
import play.data.*;
import org.bson.types.ObjectId;
import play.mvc.Http.*;


public class Application extends Controller {
  
 //my Forms at this rate the number of forms could be exponential// ;-) //is htis normal?
  static Form<Post> postForm=form(Post.class);
  static Form<Comment> commentForm=form(Comment.class);
  public static Form<User> signupForm=form(User.class);
 public static Form<User> loginForm=form(User.class);
  public static Form<User> userForm=form(User.class);



 public static Result login(){
    Form<User> filledForm=loginForm.bindFromRequest();
    System.out.print(filledForm);
    if(filledForm.hasErrors()){
      flash("error","Login failed");
        return redirect(routes.Application.posts());
    }else{
        String authenticatedId=User.authenticate(filledForm.field("email").value(),filledForm.field("password").value());
        System.out.println(authenticatedId);
        if(authenticatedId!=null){
          session("auth_user",authenticatedId);
          flash("my_message","Login successful");
          return redirect(routes.Application.posts());
        }else{
          flash("error","Invalid Credentials");
          return redirect(routes.Application.posts());
        }
    }
  }
  public static User getAuthUser(){
    return User.getUserById(ObjectId.massageToObjectId(session("auth_user")));
  }
  public static Result logout(){
    session().clear();
    return redirect(routes.Application.index());
  }
  public static Result index() {
    return ok(views.html.index.render(loginForm));
  }
 
  
  //TODO refacot signup validation should be streamilined and simpler plus proper erro mechs used
  public static Result signup(){
    if(request().method()=="POST"){
      Form<User> filledForm=signupForm.bindFromRequest();
      System.out.println("Form submitted for signup "+filledForm);
      boolean passwordConfirmed=filledForm.field("password").value().length()>0&&(filledForm.field("password").value().equals(filledForm.field("confirmPassword").value()));
      boolean emailNotValid=filledForm.field("email").value().length()<6;
      System.out.println("password:"+passwordConfirmed+"- email:"+emailNotValid);
      if(filledForm.hasErrors() || emailNotValid || !(passwordConfirmed)){
        filledForm.reject("Signup failed please check and resubmit");
          return badRequest(views.html.signupForm.render(filledForm));  
      }//no errors so save user and redirect to index
        User.create(filledForm.get());
        
        session("auth_user",User.authenticate(filledForm.field("email").value(),filledForm.field("password").value()));
        flash("my_message","Signup was successful");
        return redirect(routes.Application.posts());
    }
    //GET request so show form
    
     return ok(views.html.signupForm.render(signupForm));
  }

  public static Result profile(String id ){
    return ok(views.html.profile.render(User.getUserById(ObjectId.massageToObjectId(id))));
  }

  public static Result updateProfile(String id){
    User user= User.find().byId(ObjectId.massageToObjectId(id));
    System.out.println("User:"+user+" _"+user.id);
    if(request().method()=="GET"){
      System.out.println("get request");
     Form<User> prefilledForm = userForm.fill(user);
            return ok(views.html.updateProfileForm.render(id, prefilledForm));        
      }else{
        System.out.println("post request?");
          Form<User> filledForm=userForm.bindFromRequest();
          //System.out.println(filledForm.get());
          System.out.println(filledForm);
          if(filledForm.hasErrors() ){
              return badRequest(views.html.updateProfileForm.render(id,filledForm));  
          }//no errors so save user and redirect to index
          //filledForm.get().update(user.id);
          User.modify(filledForm.get(),id);
          flash("my_message","Profile Updated");
          return redirect(routes.Application.profile(id));
    }
    //GET request so show form

  }

public static Result doMgQuestionaire(){
  QuestionSet questionSet=QuestionSet.getQuestionSetByTitle("Music Group Application");
  if(questionSet.title==null){
    flash("my_message","Music Group Application is not available");
    return redirect( "/posts" );
  }
    return redirect( routes.Management.doQuestionSet(questionSet.id.toString()));
  }
 
public static Result posts(){

    return ok(posts.render(Post.all(),commentForm));
  }
public static Result post(String id){
  return ok(post.render(Post.getPost(ObjectId.massageToObjectId(id))));
}
    public static Result postForm(){

    return ok(views.html.postForm.render(postForm));
  }

public static Result listPosts(){
  return ok(views.html.listPosts.render(Post.all()));
}

  public static Result newPost(){
    Form<Post> filledForm=postForm.bindFromRequest();
    if(filledForm.hasErrors()){
      return badRequest(views.html.postForm.render(filledForm));
    }else{
      Post.create(filledForm.get());
      return redirect(routes.Application.posts());
    }

  }
   public static Result editPost(String id){
    Post post=Post.getPost(ObjectId.massageToObjectId(id));
    if(request().method()=="POST"){
      Form<Post> filledForm=postForm.bindFromRequest();
      if(filledForm.hasErrors()){
        return badRequest(views.html.editPostForm.render(post,filledForm));  
      }else{
        Post.modify(filledForm.get(),post.id);
        flash("my_message","Post Updated");
        return ok(views.html.posts.render(Post.all(),commentForm));
      }
    }
    Form<Post> prefilledForm = postForm.fill(post);
            return ok(views.html.editPostForm.render(post, prefilledForm));        
      //return ok(views.html.editQuestionForm.render(Question.getQuestion(ObjectId.massageToObjectId(id)),questionForm));
  }
  public static Result newComment(String id){

    Post be=Post.getPost(ObjectId.massageToObjectId(id));
    Form<Comment> filledForm=commentForm.bindFromRequest();
    if(filledForm.hasErrors()){
      return badRequest(posts.render(Post.all(),commentForm));
    }else{
      System.out.print(filledForm.get());
      System.out.println(filledForm.get());
      be.insertComment(filledForm.get());
      return redirect(routes.Application.posts());
    }

  }

}