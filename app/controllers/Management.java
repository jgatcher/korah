package controllers;

import play.*;
import play.mvc.*;
import models.*;
import views.html.*;
import play.data.*;
import org.bson.types.ObjectId;
import play.mvc.Http.*;
import java.util.*;

public class Management extends Controller{
	
//Forms
static Form<Question> questionForm=form(Question.class);
static Form<QuestionSet> questionSetForm=form(QuestionSet.class);
static Form<QuestionMapping> questionMapForm=form(QuestionMapping.class);
static Form<QResponse> responseForm=form(QResponse.class);
static Form<ResponseSet> responseSetForm=form(ResponseSet.class);
static Form<QuestionMappingList> questionMappingListForm=form(QuestionMappingList.class);



//Actions
  
public static Result profiles(){
    return TODO;
  }
  public static Result index(){
    return TODO;
  }
  
  public static Result questionForm(){
    return ok(views.html.questionForm.render(questionForm));
  }
  public static Result questions(){

    return ok(views.html.questions.render(Question.all()));
  }

public static String sidePaneLogic(String path){
  
  return "<i>hello</i>";

}

  public static Result editQuestion(String id){
    Question question=Question.getQuestion(ObjectId.massageToObjectId(id));
    if(request().method()=="POST"){
      Form<Question> filledForm=questionForm.bindFromRequest();
      if(filledForm.hasErrors()){
        return badRequest(views.html.editQuestionForm.render(question,filledForm));  
      }else{
        Question.modify(filledForm.get(),question.id);
        flash("my_message","Question Updated");
        return ok(views.html.questions.render(Question.all()));
      }
    }
    Form<Question> prefilledForm = questionForm.fill(question);
            return ok(views.html.editQuestionForm.render(question, prefilledForm));        
      //return ok(views.html.editQuestionForm.render(Question.getQuestion(ObjectId.massageToObjectId(id)),questionForm));
  }

  public static Result getResponseSet(String id){
      ObjectId oid=ObjectId.massageToObjectId(id);
      return ok(views.html.responseSet.render(ResponseSet.getResponseSet(oid)));

  }
  public static Result responseSets(){
    return ok(views.html.responseSets.render(ResponseSet.all()));
  }

    public static Result setupQuestionSet(String id){
    
    System.out.println("setup function called request was"+request().body()+"\n");
    QuestionSet questionSet= QuestionSet.find().byId(ObjectId.massageToObjectId(id));
    
    if(request().method()=="POST"){
       
       Form<QuestionSet> filledQuestionSet=questionSetForm.bindFromRequest();
       Form<Question> filledQuestionForm=questionForm.bindFromRequest();

       if(filledQuestionForm.field("createAndSave").value()!=null){
           
          
          if(filledQuestionForm.hasErrors()) 
            return badRequest(views.html.setupQuestionSet.render(questionSet,Question.all(),filledQuestionForm));  
          System.out.println(filledQuestionForm);
          ObjectId createdQuestionId=Question.createAndReturnId(filledQuestionForm.get());
          System.out.println("Next question No is:"+questionSet.nextQuestionNo()+"\n Object id is"+createdQuestionId);
          questionSet.addQuestion(questionSet.nextQuestionNo(),createdQuestionId);
       
       }else if(filledQuestionSet.field("addMultipleQuestions").value()!=null){
             //System.out.println(filledQuestionSet("list[].questionNo").value());
           System.out.println("QuestionSet:\t"+filledQuestionSet);
           if(filledQuestionSet.hasErrors())
              return badRequest(views.html.setupQuestionSet.render(questionSet,Question.all(),filledQuestionForm));  
           
            System.out.print(filledQuestionSet.get());
           //  SortedMap <Integer,ObjectId> qm=filledQuestionSet.get().attachedQuestions;
           //  Set <Integer>keys=qm.keySet();
           // Integer[] arr= {};
           // arr=keys.toArray(arr);
           // List <Integer> list=Arrays.asList(arr);
           // Collections.sort(list);
           // System.out.println("the list"+list);
           //  for(Integer i:list){
           //    questionSet.addQuestion(i,qm.get(i));
           //  }
           questionSet.addQuestions(filledQuestionSet.get().attachedQuestions);

       }

    }     
    return ok(views.html.setupQuestionSet.render(questionSet,Question.all(),questionForm));

  }
  

  static Form<Simple> simpleform=form(Simple.class);
  public static Result simpleForm(){
    
    Form<Simple> filledForm=simpleform.bindFromRequest();
    System.out.println(filledForm);
    return ok(views.html.simpleForm.render(filledForm.get().toString()));
  }

  public static Result doQuestionSet(String id){
    System.out.println("setup function called request was"+request().body()+"\n");
    
    ObjectId oid=ObjectId.massageToObjectId(id);
    QuestionSet questionSet=QuestionSet.getQuestionSet(oid);
    
    if (questionSet.attachedQuestions==null||questionSet.attachedQuestions.size()==0){
       
       flash("my_message","'"+questionSet.title+"' is an Empty Question Set,click setup to add questions to it");
      return  redirect(routes.Management.questionSets());    
    }
      if(request().method()=="POST"){
        Form<ResponseSet> filledForm=responseSetForm.bindFromRequest();
        System.out.println("Your filled Form:\n"+filledForm);
        
        ObjectId responseSetId=ResponseSet.createAndReturnId(filledForm.get());
        flash("my_message","Responses Saved");
         // return ok(views.html.responseSet.render(ResponseSet.getResponseSet(responseSetId)));
        return  redirect(routes.Management.questionSets());    
    }
    return ok(views.html.doQuestionSet.render(questionSet,responseSetForm));

  }
  //Helper method checks if option is slelected and returns appropriate html "code"
  //doing htis in the view would have beeen horendous
 public static String getOptionState(String option,List<String> options){
    

    for(String s:options){
      if(option!=null && option.equals(s)){
        return "checked";
      }
      
    }return "not checked";
  }
  
  public static Result editResponses(String id){
    ObjectId oid=ObjectId.massageToObjectId(id);
    ResponseSet responseSet=ResponseSet.getResponseSet(oid);
    QuestionSet questionSet=QuestionSet.getQuestionSet(responseSet.questionSetId);
    // if (questionSet.attachedQuestions==null||questionSet.attachedQuestions.size()==0){
       
    //    flash("my_message","'"+questionSet.title+"' is an Empty Question Set,click setup to add questions to it");
    //   return  redirect(routes.Management.questionSets());    
    // }
      if(request().method()=="POST"){
        Form<ResponseSet> filledForm=responseSetForm.bindFromRequest();
        System.out.println(filledForm);
        
        ResponseSet modifiedResponseSet=ResponseSet.modify(filledForm.get(),oid);
        flash("my_message","Responses Saved");
         return ok(views.html.responseSet.render(modifiedResponseSet));
    }
    return ok(views.html.editResponses.render(questionSet,responseSet,responseSetForm));

  }
  public static Result previewQuestion(String id){
    ObjectId oid=ObjectId.massageToObjectId(id);
    if(request().method()=="POST"){
      Form<QResponse> filledForm=responseForm.bindFromRequest();
    }
    return ok(views.html.previewQuestion.render(Question.getQuestion(oid),responseForm));
  }
 // public static Result doQuestionSet(String id){
 //    ObjectId oid=ObjectId.massageToObjectId(id);
 //    return ok(views.html.doQuestionSet.render(QuestionSet.getQuestionSet(oid)));
 // }
  public static Result newQuestion(){
    if(request().method()=="POST"){
      Form<Question> filledForm=questionForm.bindFromRequest();
      if(filledForm.hasErrors()) {
          return badRequest(views.html.questionForm.render(filledForm));  
      }//no errors so save user and redirect to index
        Question.create(filledForm.get());
        flash("my_message","Question created");
        return redirect(routes.Management.questions());
    }
    //GET request so show form
         return ok(views.html.questionForm.render(questionForm));
    
  }

  public static Result newQuestionSet(){
    if(request().method()=="POST"){
      Form<QuestionSet> filledForm=questionSetForm.bindFromRequest();
      if(filledForm.hasErrors()) {
        return badRequest(views.html.questionSetForm.render("NA",filledForm));  //fake id
      }//no errors so save user and redirect to index
        QuestionSet.create(filledForm.get());
        flash("my_message","Question Set created");
        return redirect(routes.Management.questionSets());
    }
    return ok(views.html.questionSetForm.render("NA",questionSetForm));
  }

  public static Result questionSets(){
    return ok(views.html.questionSets.render(QuestionSet.all()));
  }

  public static Result editQuestionSet(String id){
    QuestionSet questionSet= QuestionSet.find().byId(ObjectId.massageToObjectId(id));
    if(request().method()=="GET"){
      System.out.println("get request");
      Form<QuestionSet> prefilledForm = questionSetForm.fill(questionSet);
            return ok(views.html.editQuestionSetForm.render(id, prefilledForm));        
      }else{
         Form<QuestionSet> filledForm=questionSetForm.bindFromRequest();
          if(filledForm.hasErrors() ){
              return badRequest(views.html.editQuestionSetForm.render(id,filledForm));  
          }//no errors so save user and redirect to index
          //filledForm.get().update(user.id);
           QuestionSet.modify(filledForm.get(),ObjectId.massageToObjectId(id));
          flash("my_message","QuestionSet Updated");
          return redirect(routes.Management.questionSets());
    }
  }

  


  public static Result getQuestionSet(String id){
    return TODO;
  }
  

	
}
