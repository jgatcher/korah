import sbt._
import Keys._
import PlayProject._

object ApplicationBuild extends Build {

    val appName         = "korah2"
    val appVersion      = "1.0-SNAPSHOT"

    val appDependencies = Seq(
      // Add your project dependencies here,
      //collections-generic was to allow the use of lazymaps etc
      "leodagdag"                         % "play2-morphia-plugin_2.9.1"  % "0.0.6",
       "net.sourceforge.collections" % "collections-generic" % "4.01"
            
    )

    val main = PlayProject(appName, appVersion, appDependencies, mainLang = JAVA).settings(
      // Add your own project settings here  
      resolvers ++= Seq(DefaultMavenRepository, Resolvers.githubRepository, Resolvers.morphiaRepository)
      ,lessEntryPoints <<= baseDirectory(customLessEntryPoints)  
    )
    object Resolvers {
      val githubRepository = "LeoDagDag repository" at "http://leodagdag.github.com/repository/"
      val morphiaRepository = "Morphia repository" at "http://morphia.googlecode.com/svn/mavenrepo/"
	}
  // Only compile the bootstrap bootstrap.less file and any other *.less file in the stylesheets directory
    def customLessEntryPoints(base: File): PathFinder = (
        (base / "app" / "assets" / "stylesheets" /"bootstrap" ** "bootstrap.less") +++
        (base / "app" / "assets" / "stylesheets" /"bootstrap" ** "responsive.less")
    )


}
